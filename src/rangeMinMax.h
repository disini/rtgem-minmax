// ======================================================================== //
// Copyright 2018 Ingo Wald                                                 //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include <algorithm>

namespace rangeMinMax {

  template<typename T>
  struct MinQuery {
    typedef T ScalarT;
    
    inline MinQuery()
      : result(MinQuery<T>::empty())
    {}
    inline MinQuery(const ScalarT &s)
      : result(s)
    {}
    inline MinQuery(const MinQuery &range, const MinQuery &other)
      : result(std::min(range.result,other.result))
    {}
    /*! what we return for a completely empty range */
    static ScalarT empty();
    ScalarT result;
  };

  template<> float MinQuery<float>::empty() { return std::numeric_limits<float>::infinity(); }
  

  template<typename T>
  inline bool operator!=(const MinQuery<T> &a, const MinQuery<T> &b)
  { return a.result != b.result; }


  // ==================================================================
  /*! naive reference implementation that just loops from lo to
      including hi (with O(0) storage and O(N) query cost) */
  template<typename RangeT>
  struct Reference {
    Reference(const typename RangeT::ScalarT *value, size_t N)
      : value(value), N(N)
    {}

    /*! compute range for indices 'lo' to (including" index 'hi' */
    RangeT operator()(const size_t lo, const size_t hi)
    {
      RangeT range;
      for (size_t i=lo;i<=hi;i++)
        range = RangeT(range,value[i]);
      return range;
    }  
    
  private:
    const typename RangeT::ScalarT *value;
    size_t N;
  };



  // ==================================================================
  /*! O(1) query variant that uses precomputed tables for every
      power-of-two sized sub-ranges */
  template<typename RangeT>
  struct SparseTables {
    SparseTables(const typename RangeT::ScalarT *value, size_t N)
      : value(value), N(N)
    {
      // for level 0 we don't have to store anything, because that's
      // the input array itself
      sparseTablePerPot.push_back(std::vector<RangeT>());
      for (size_t bits=1;(1<<bits)<=N;bits++) 
        sparseTablePerPot.push_back(computeTableEntry(bits));
    }

    std::vector<RangeT> computeTableEntry(size_t bits) const
    {
      std::vector<RangeT> r;
      if (bits == 1) {
        for (size_t i=0;i<N-1;i++)
          r.push_back(RangeT(value[i],value[i+1]));
      } else {
        size_t pot = 1<<bits;
        for (size_t i=0;(i+pot)<=N;i++)
          r.push_back(RangeT(sparseTablePerPot[bits-1][i],
                             sparseTablePerPot[bits-1][i+pot/2]));
      }
      return r;
    }
    
    /*! compute range for indices 'lo' to (including" index 'hi' */
    RangeT operator()(const size_t lo, const size_t hi)
    {
      if (lo > hi)  return RangeT();
      if (lo == hi) return RangeT(value[lo]);

      size_t pot = 0;
      size_t width = (hi-lo+1);
      while (2*(1<<pot) <= width) ++pot;

      size_t NinPot = (1<<pot);
      return RangeT(sparseTablePerPot[pot][lo],
                    sparseTablePerPot[pot][hi+1-NinPot]);
    }  

    size_t memoryUsed()
    {
      size_t mem = 0;
      for (int i=0;i<sparseTablePerPot.size();i++) {
        mem += sizeof(std::vector<RangeT>);
        mem += sparseTablePerPot[i].size()*sizeof(sparseTablePerPot[i][0]);
      }
      return mem;
    }
  
  private:
    /*! our sparse table vector - for each power of two less than N we
        store a table for each possible {i,i+N] query */
    std::vector<std::vector<RangeT>> sparseTablePerPot;
    const typename RangeT::ScalarT *value;
    size_t N;
  };

  
  // ==================================================================
  /*! our iterative range tree traversal, using a binary range tree /
      segment tree built over the input array, and using our
      non-recursive, iterateive bottom-up traversal (with O(N) storage
      and O(log N) traversal) */
  template<typename RangeT>
  struct IterativeRangeTree {
    /*! constructor - builds the tree */
    IterativeRangeTree(const typename RangeT::ScalarT *value, size_t N)
      : value(value),
        N(N)
    {
      // create root level
      for (size_t i=0;i<N/2;i++) 
        nodes.push_back(RangeT(value[2*i],value[2*i+1]));
      
      // create all inner levels
      size_t levelBegin = 0;
      size_t  num_in = N/2;
      while (num_in >= 2) {
        for (size_t i=0;i<num_in/2;i++) 
          nodes.push_back(RangeT(nodes[levelBegin+2*i],nodes[levelBegin+2*i+1]));
        levelBegin += num_in;
        num_in /= 2;
      }
    }

    /*! compute range for indices 'lo' to (including" index 'hi' */
    inline RangeT operator()(const size_t _lo, const size_t _hi)
    {
      int64_t lo = _lo;
      int64_t hi = _hi;
      RangeT range;
      if (lo <= hi) {
        /* the first traverrsal step, manualy unrolled because unlike
           all other steps this one operates on the 'value' array (all
           inner steps operate on 'node' array) */
        if ((lo & 1) == 1) range = RangeT(range,value[lo]);
        if ((hi & 1) == 0) range = RangeT(range,value[hi]);
        lo = (lo+1)>>1;
        hi = (hi-1)>>1;

        /* from now on, all traversal steps are on inner nodes */
        RangeT *node = &nodes[0];
        size_t  num_nodes = N/2;
        while (lo <= hi) {
          if ((lo & 1)) range = RangeT(range,node[lo]);
          if (!(hi & 1)) range = RangeT(range,node[hi]);
          // lo = (lo+1)/2;
          // hi = (hi+1)/2-1;
          lo = (lo+1)>>1;
          hi = (hi-1)>>1;

          // aaaaand .... step to next level...
          node += num_nodes;
          num_nodes /= 2;
        }
      }
      return range;
    }

    size_t memoryUsed()
    {
      size_t mem = 0;
      mem += nodes.size() * sizeof(nodes[0]);
      return mem;
    }
  
    /*! the range tree, linearized into a single linear
        std::vector. The first 0..N/2 nodes the lowest level (built
        over the N input values), the next N/2/2 the next level,
        etc */
    std::vector<RangeT> nodes;
    /*! the original input array we're performing the queries in */
    const typename RangeT::ScalarT *value;
    size_t N;
  };
  
}
