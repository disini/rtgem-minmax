// ======================================================================== //
// Copyright 2018 Ingo Wald                                                 //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "platform.h"
#include <vector>
#include <math.h>
#include <sys/times.h>
#include "rangeMinMax.h"

struct Gaussian
{
  Gaussian(float mue, float sigmaSqr) : mue(mue), sigmaSqr(sigmaSqr) {};

  float operator()(float x)
  {
    float g =  1.f/sqrt(2*M_PI*sigmaSqr)*exp(-(x-mue)*(x-mue)/(2*sigmaSqr));
    return g;
  }
  float mue, sigmaSqr;
};
  
/*! generate a test-data set for the given size and type, using a
    distribution of three gaussians, scaled by the number of elements
    in the array, N (to work for int types, too */
std::vector<float> generateTestDataSet(size_t N)
{
  std::vector<float> values(N);
  Gaussian g0(.3,.01);
  Gaussian g1(.6,.0025);
  Gaussian g2(.8,.003);
  for (size_t i=0;i<N;i++) {
    float f = (i+.5)/float(N);
    float w = g0(f)+g1(f)+g2(f);
    values[i] = w;
  }
  return values;
}

/*! run a series of all benchmarks */
template<typename QueryT = rangeMinMax::MinQuery<float>>
void benchmark_one_size(size_t N)
{
  std::cout << "benchmarking N=" << N << std::endl;
  
  std::vector<float> testData = generateTestDataSet(N);
  // ------------------------------------------------------------------
  // generate a given distribution
  // ------------------------------------------------------------------
  std::vector<float> values(N);
  Gaussian g0(.3,.01);
  Gaussian g1(.6,.0025);
  Gaussian g2(.8,.003);
  for (size_t i=0;i<N;i++) {
    float f = (i+.5)/float(N);
    float w = g0(f)+g1(f)+g2(f);
    values[i] = float(N*w);
    // std::cout << " " << i << " -> " << values[i] << std::endl;
  }

  rangeMinMax::Reference<QueryT> reference(&values[0],values.size());
  rangeMinMax::IterativeRangeTree<QueryT> rangeTree(&values[0],values.size());
  rangeMinMax::SparseTables<QueryT> sparseTables(&values[0],values.size());
  // rangeMinMax::Method2 method2(&values[0],values.size());

  static size_t t_ref = 0;
  static size_t t_st  = 0;
  static size_t t_rt  = 0;
  
  size_t numExperiments = 100000;
  
  srandom(123456ULL);
  for (size_t run=0;run<numExperiments;run++) {
#if 1
    // enforcing queries that are at most 2*sqrt(n) wide
    int i0 = random() % N;
    int d = int(drand48()*sqrtf(N));
    int lo = std::max((int)0,(int)i0-d);
    int hi = std::min((int)N-1,i0+d);
#else
    // unifromly distributed query endpoints, often leading to very
    // large (and arguably less represenative) queries
    size_t i0 = random() % N;
    size_t i1 = random() % N;
    size_t lo = std::min(i0,i1);
    size_t hi = std::max(i0,i1);
#endif

    t_ref -= times(NULL);
    QueryT v_ref = reference(lo,hi);
    t_ref += times(NULL);

    t_rt -= times(NULL);
    QueryT v_rt = rangeTree(lo,hi);
    t_rt += times(NULL);

    t_st -= times(NULL);
    QueryT v_st = sparseTables(lo,hi);
    t_st += times(NULL);

    if (v_ref != v_rt) {
      PING;
      PRINT(v_ref.result);
      PRINT(v_rt.result);
      throw std::runtime_error("mismatch for range tree method!");
    }
    if (v_ref != v_st) {
      PING;
      PRINT(v_ref.result);
      PRINT(v_st.result);
      throw std::runtime_error("mismatch for sparse table method!");
    }
  }
  std::cout << "T_ref=" << t_ref << " T_sparseTables=" << t_st << " (mem=" << sparseTables.memoryUsed() << "B), T_iterativeRangeTree=" << t_rt << " (mem=" << rangeTree.memoryUsed() << "B)" << std::endl;
}
  
/*! run a series of all benchmarks */
void benchmark_all_sizes()
{
  for (size_t size=1; size<10000000; size=(size<10?size+1:size_t(size*1.1f)))
    benchmark_one_size(size);
}

int main(int, char **)
{
  benchmark_all_sizes();
}
